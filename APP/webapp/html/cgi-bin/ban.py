#!/usr/bin/python3
# -*- coding: utf-8 -*-
import cgi
import cgitb
#import LBC_advancedSearch
import sys
sys.path.append('/usr/local/lib/python3.8/site-packages')
import yaml
import os
cgitb.enable()

print("Content-type: text/html\n\n")

form=cgi.FieldStorage()

search=form["search"].value
list_id=form["list_id"].value
YMLFILENAME=form["ymlfile"].value
OUTPUTFILENAME=YMLFILENAME + "_tmp"
print("L'annonce " + list_id + " a &eacute;t&eacute; banni de la recherche : " + search + "</br></br>")

YMLFile = open(YMLFILENAME, 'r+', encoding='utf-8')
INPUT = yaml.safe_load(YMLFile)

OUTPUTFile = open(OUTPUTFILENAME, 'w', encoding='utf-8')
for args in INPUT["searchname"]:
  for thissearch in str.split(args["name"]):
    if thissearch == search:
      ban = args["ban"]
      ban.append(int(list_id))
      yaml.dump(INPUT, OUTPUTFile,  default_flow_style=False, sort_keys=False)
YMLFile.close()
os.replace(OUTPUTFile.name, YMLFile.name)