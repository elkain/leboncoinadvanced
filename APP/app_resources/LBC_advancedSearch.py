#!/usr/bin/python3
# -*- coding: utf-8 -*-
####################################################################################
####          Usage       ####
####################################################################################

"""LBC_advancedSearch.py

Usage:
  LBC_advancedSearch.py --file=<file>

Options:
  -f <INPUTFILE> --file=<INPUTFILE>                       Fichier contenant les mot clé à rechercher.
"""
####################################################################################
####          Import        ####
####################################################################################

import json
import numpy as np
from requests_html import HTMLSession
from datetime import datetime
import xml.etree.ElementTree as ET
import http.server
# from http.server import HTTPServer, BaseHTTPRequestHandler
import socketserver
from docopt import docopt
import yaml
from jinja2 import Environment, FileSystemLoader
import os
import time
from urllib.parse import parse_qs

# TYPE="debug"

PORT = 8080
Handler = http.server.CGIHTTPRequestHandler

args = docopt(__doc__)

session = HTMLSession()
now = datetime.now()
now = now.strftime("%Y-%m-%dT%H:%M:%SZ")


newdict = {}
searchs = {}
result = {}

try:
  if str(TYPE) == "debug":
    PATH = "APP/"
    PATH2 = PATH + "app_resources/"
    OSRM_SERVER = "127.0.0.1"
    LOGFILE = open("LOG.log","w", encoding='utf-8')
except:
  PATH = "/"
  PATH2 = PATH + "app/"
  OSRM_SERVER = "osrm"
  LOGFILE = ""
  
#Lecture des arguments dans un fichiers YML
YMLFILENAME = PATH2 + str(args["--file"]) 
YMLFile = open(YMLFILENAME,"r", encoding='utf-8')
INPUT = yaml.safe_load(YMLFile)
try:
  if str(TYPE) != "debug":
    os.chown(PATH2, 65534, 0)
    os.chown(YMLFILENAME, 65534, 0)
    os.chmod(YMLFILENAME, 0o770)
except:
  print("You have to set the owner according to your system's user")
  
for args in INPUT["searchname"]:
  words = []
  for word in args["searchwords"]:
    words.append(word)
  searchs[args["name"]] = words
YMLFile.close()
try:
  if str(TYPE) == "debug":
    CSVFILENAME = str(args["output"][0] + ".csv")
except:
  CSVFILENAME = ""

webpath = os.path.dirname(os.path.abspath(__file__))
templates_dir = os.path.join(PATH + 'webapp/templates')
env = Environment( loader = FileSystemLoader(templates_dir), extensions=['jinja2.ext.loopcontrols'])
indextemplate = env.get_template('index.j2')
resulttemplate = env.get_template('searchresult.j2')

indexfilename = os.path.join(PATH + 'webapp/html', 'index.html')

with open(indexfilename, 'w') as fh:
  fh.write(indextemplate.render(
    searchs = searchs
  ))

for args in INPUT["searchname"]:
  #Set une nouvelle e[titre]ntree dans le dictionnaire de resultat
  result["name"] = str(args["name"])
  result[args["name"]] = {}


  #Headers pour la requete d'interogation de leboncoin.fr
  headersLBC = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0',
    'Accept': '*/*',
    'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
    'Referer': 'https://www.leboncoin.fr/recherche/?text=pickup',
    'api_key': 'ba0c2dad52b3ec',
    'Content-Type': 'application/json',
    'Origin': 'https://www.leboncoin.fr',
    'DNT': '1',
    'Connection': 'keep-alive',
  }

  #Si subject définis alors la recherche ne tournera que sur le titre de l'annonce
  try:
    if str(args["subject"]):
      searchtype = "subject"
  except:
      searchtype = ""
  
  #Calcul pour prix
  try:
    prix = int(args["prix"][0])
    prixmax = prix+prix/5
    prixmin = prixmax/3
  except KeyError:
    prixmax = 1000000000
    prixmin = 1
  tabprix = np.linspace(prixmax,prixmin,200,dtype=int)
  tabnoteprix = np.linspace(-100,100,200,dtype=int)
  
  #Calcul pour année (Vehicule)
  anneenow = datetime.now().year
  try:
    annee = int(args["annee"][0])
    anneemax = anneenow
    anneemin = annee-(int(anneemax)-annee)
  except KeyError:
    anneemax = anneenow
    anneemin = 1900
  tabanneeextrem = (anneemax - anneemin)/2
  stepannee = anneemax-anneemin
  tabannee = np.linspace(anneemin,anneemax,stepannee+1,dtype=int)
  tabnoteannee = np.linspace(-tabanneeextrem,tabanneeextrem,stepannee+1,dtype=int)
  
  #Calcul pour kilometre (Vehicule)
  try:
    kilometre = int(args["kilometre"][0])
    kmmax = kilometre+(kilometre/2)
    kmmin = kilometre-(kilometre/2)
    kmexl = kmmin/2
  except KeyError:
    kmmax = 250000
    kmmin = 1
  tabkm = np.linspace(int(kmmax),int(kmmin),200,dtype=int)
  tabnotekm = np.linspace(-100,100,200,dtype=int)
  
  #Caclul pour temps entre lieux choisi et annonce
  try:
    maxtime = args["maxtime"][0]
    maxtime = str(maxtime).replace(',','.')
    maxtime = float(maxtime)*3600
  except KeyError:
    maxtime = 10000000000
  tabtime = np.linspace(int(maxtime),0,100)  
  tabnotetime = np.linspace(0,100,100)  
  
  #Calcul pour Surface (Immobilier)
  try:
    superficie = int(args["superficie"][0])
    superfmax = superficie+(superficie/4)
    superfeca = superfmax-superficie
    superfmin = superficie-superfeca
    superfexl = superficie-(superficie/4)
    tabnotesup = np.linspace(-100,100,200,dtype=int)
  except KeyError:
    superfmax = 1000
    superfmin = 1
    tabnotesup = np.linspace(0,100,200,dtype=int)
  tabsup = np.linspace(int(superfmin),int(superfmax),200,dtype=int)
  
  #Calcul pour nombre de piece
  try:
    piece = int(args["piece"][0])
    piecemax = int(piece)*2
    pieceeca = int(piecemax-piece)
    piecemin = int(piece-pieceeca)
  except KeyError:
    piecemax = 50
    piecemin = 1
  tabpieceextrem = (piecemax - piecemin)/2
  steppiece = piecemax-piecemin
  tabpiece = np.linspace(piecemin,piecemax,steppiece+1,dtype=int)
  tabnotepiece = np.linspace(-tabpieceextrem,tabpieceextrem,steppiece+1,dtype=int)
  
  ####################################################################################
  ####          BODY          ####
  ####################################################################################
  new_jsondata = {}
  #Preparation puis envoie de la requete HTTPS vers leboncoin.fr pour chaque mot clé
  loop = 10
  while loop > 0:
    offset = int(loop*40)
    try:
      searchwords = args["searchwords"]
    except KeyError:
      searchwords = {""}
      searchtype = ""
    for searchword in searchwords:
      dataLBC = '{"filters":{"category":{},"enums":{"ad_type":["offer"]},"keywords":{"text":"'+ str(searchword) + '","type":"'+ str(searchtype) +'"},"location":{},"ranges":{}},"limit":100,"limit_alu":3,"offset":'+ str(offset) +',"user_id":"85905f9c-25f8-49ce-8848-3664f6b305ee","store_id":"24271536"}'
      r = session.post('https://api.leboncoin.fr/finder/search', headers=headersLBC, data=dataLBC, allow_redirects=False)
      json_result = r.json()
      try:
        for item in json_result["ads"]:
          list_id = item['list_id']
          new_jsondata[list_id] = item
      except:
        continue  
    loop = loop-1
    
  if CSVFILENAME != "":
    with open(CSVFILENAME,"w", encoding='utf-8') as CSVFile:
      CSVFile.write("titre;prix;marque;modele;annee;date de mise en circulation;kilometrage;carburant;boite de vitesse;detail;type de bien;type de vente;nombre de pieces;surface;ville;departement;region;lien;note;note annee;note km;note prix;note temps;note surface;note piece\n" )
  
  orig = ""
  origs = ""
  orig_id = 0
  dataLOC = ""
  sourceLST = ""
  for orig in args["origloc"]:
    orig["id"] = orig_id
    sourceLST = sourceLST + ";" + str(orig_id)
    orig_id += 1
    dataLOC = dataLOC + ";" + orig["lng"] + "," + orig["lat"]
  dataLOC = dataLOC[1:]
  sourceLST = str(sourceLST[1:])
  destLST = str(orig_id)

  for list_id in new_jsondata:
    if list_id in args["ban"]:
      if LOGFILE != "":
        LOGFILE.write(str(list_id) + " BAN\n")
      continue
    result[args["name"]][list_id] = {}
    result[args["name"]][list_id]["list_id"] = list_id
    titre = ""
    image = ""
    prix = ""
    marque = ""
    modele = ""
    annee = ""
    miseencircu = ""
    kilometrage = ""
    carburant = ""
    boitevitesse = ""
    detail = ""
    ville = ""
    departement = ""
    region = ""
    lien = ""
    note = 0
    noteprix = 0
    noteannee = 0
    notekm = 0
    notetime = 0
    notepiece = 0
    notesurface = 0
    times = ""
    exclude = 0
    typedebien = ""
    typedevente = ""
    surface = 0
    pieceann = 0
    
    if 'price' in new_jsondata[list_id]:
      prix = str(new_jsondata[list_id]["price"])
      prix = prix.replace('[','')
      prix = prix.replace(']','')
      if int(prix) < int(prixmax) and int(prix) > int(prixmin):
        result[args["name"]][list_id]["prix"] = prix
        noteprix = tabnoteprix[np.where((tabprix > int(prix)) & (tabprix < int(prix)+((int(prix)/100)*4)))][0]/2
        if 'subject' in new_jsondata[list_id]:
          titre = str(new_jsondata[list_id]["subject"])
          result[args["name"]][list_id]["titre"] = titre
        else:
          if LOGFILE != "":
            LOGFILE.write(str(list_id) + " pas de titre\n")
        if 'body' in new_jsondata[list_id]:
          detail = str(new_jsondata[list_id]["body"])
          detail = (detail[:3000] + '...') if len(detail) > 3000 else detail
          result[args["name"]][list_id]["detail"] = detail
        else:
          if LOGFILE != "":
            LOGFILE.write(str(list_id) + " pas de detail\n")
        if 'url' in new_jsondata[list_id]:
          lien = str(new_jsondata[list_id]["url"])
          result[args["name"]][list_id]["lien"] = lien
        else:
          if LOGFILE != "":
            LOGFILE.write(str(list_id) + " pas de URL\n")
        if 'location' in new_jsondata[list_id]:
          if 'region_name' in new_jsondata[list_id]["location"]:
            region = str(new_jsondata[list_id]["location"]["region_name"])
            result[args["name"]][list_id]["region"] = region
          else:
            if LOGFILE != "":
              LOGFILE.write(str(list_id) + " pas de region\n")
          if 'department_name' in new_jsondata[list_id]["location"]:
            departement = str(new_jsondata[list_id]["location"]["department_name"])
            result[args["name"]][list_id]["departement"] = departement
          else:
            if LOGFILE != "":
              LOGFILE.write(str(list_id) + " pas de departement\n")
          if 'city' in new_jsondata[list_id]["location"]:
            ville = str(new_jsondata[list_id]["location"]["city"])
            result[args["name"]][list_id]["ville"] = ville
          else:
            if LOGFILE != "":
              LOGFILE.write(str(list_id) + " pas de ville\n")
          if 'lat' in new_jsondata[list_id]["location"]:
            times = []
            
            rloc = session.get('http://' + OSRM_SERVER + ':5000/table/v1/driving/' + dataLOC + ';' + str(new_jsondata[list_id]["location"]["lng"]) + ',' + str(new_jsondata[list_id]["location"]["lat"]) + '?sources=' + sourceLST + '&destinations=' + destLST)
            json_loc = rloc.json()
            newtime = None
            timeperloc = {}
            loc_id = 0
            if json_loc["code"] == "Ok":
              if json_loc["destinations"][0]["name"] != "":
                for calctime in json_loc["durations"]:
                  location = args["origloc"][loc_id]["locid"]
                  newtime = int(calctime[0])
                  timeperloc[location] = int(newtime/3600)
                  times.append(str(newtime))
                  loc_id += 1
              else:
                result[args["name"]][list_id]["ban"] = True
            else:
              print("Erreur de localisation indefini" + str(rloc.status_code))
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + "erreur de localisation indefini : " + str(rloc.status_code) + "\n")
            #A Voir comment afficher le temps de trajet depuis toutes les origins
            if times:
              try:
                if str(args["locAnd"]):
                  temps = 0
                  for sum in times:
                    temps = int(temps) + int(sum)
                  temps = temps/len(times)
              except KeyError:
                if str(args["locOr"]):
                  temps = min(times)
                else:
                  temps = min(times)
              if temps:
                result[args["name"]][list_id]["temps"] = int(temps)
                if int(maxtime) <= int(temps):
                  notetime = -300
                else:
                  notetime = int(((tabnotetime[np.where((tabtime > int(temps)) & (tabtime < int(temps)+200))][0])))
          else:
            if LOGFILE != "":
              LOGFILE.write(str(list_id) + " pas de lat\n")
        else:
          print(str(list_id) + " pas de location")
        if 'thumb_url' in new_jsondata[list_id]['images']:
          image = str(new_jsondata[list_id]['images']['thumb_url'])
          result[args["name"]][list_id]["image"] = image
        else:
          if LOGFILE != "":
            LOGFILE.write(str(list_id) + " pas de image\n")
        if 'attributes' in new_jsondata[list_id]:
          for attributes in new_jsondata[list_id]["attributes"]:
            if attributes["key"] == "brand":
              marque = str(attributes["value"])
              result[args["name"]][list_id]["marque"] = marque
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de marque\n")
            if attributes["key"] == "model":
              modele = str(attributes["value"])
              result[args["name"]][list_id]["modele"] = modele
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de modele\n")
            if attributes["key"] == "regdate":
              annee = str(attributes["value"])
              result[args["name"]][list_id]["annee"] = annee
              if int(annee) > int(anneemin):
                noteannee = ((tabnoteannee[np.where((tabannee == int(annee)))][0])*100)/tabanneeextrem
              else:
                noteannee = -150
                if LOGFILE != "":
                  LOGFILE.write(str(list_id) + " anne < annee min\n")
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de date de annee\n")
            if attributes["key"] == "issuance_date":
              miseencircu = str(attributes["value"])
              result[args["name"]][list_id]["miseencircu"] = miseencircu
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de date de mise en circu\n")
            if attributes["key"] == "mileage":
              kilometrage = str(attributes["value"])
              result[args["name"]][list_id]["kilometrage"] = kilometrage
              if int(kilometrage) > int(kmmin) and int(kilometrage) < int(kmmax):
                notekm = int(tabnotekm[np.where((tabkm > int(kilometrage)) & (tabkm < int(kilometrage)+5000))][0])
              elif int(kilometrage) > int(kmmax):
                notekm = -150
              elif int(kilometrage) < int(kmmin):
                if int(kilometrage) < int(kmexl):
                  notekm = -300
                else:
                  notekm = 150
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de kilometrage\n")
            if attributes["key"] == "fuel":
              carburant = str(attributes["value_label"])
              result[args["name"]][list_id]["carburant"] = carburant
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de carburant\n")
            if attributes["key"] == "gearbox":
              boitevitesse = str(attributes["value_label"])
              result[args["name"]][list_id]["boitevitesse"] = boitevitesse
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de boite de vitesse\n")
            if attributes["key"] == "type_real_estate_sale":
              typedevente = str(attributes["value_label"])
              result[args["name"]][list_id]["typedevente"] = typedevente
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de type de vente\n")
            if attributes["key"] == "square":
              surface = str(attributes["value"])
              result[args["name"]][list_id]["surface"] = surface
              if int(surface) > int(superfmin) and int(surface) < int(superfmax):
                notesurface = int(tabnotesup[np.where((tabsup >= int(surface)) & (tabsup < int(surface)+5))][0])
              elif int(surface) < int(superfmin):
                notesurface = -150
              elif int(surface) > int(superfmax):
                notesurface = 150
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de surface\n")
            if attributes["key"] == "rooms":
              pieceann = str(attributes["value"])
              result[args["name"]][list_id]["pieceann"] = pieceann
              if int(pieceann) > int(piecemax):
                notepiece = 150
              elif int(pieceann) > int(piecemin):
                notepiece = int(((tabnotepiece[np.where((tabpiece == int(pieceann)))][0])*100)/tabpieceextrem)
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de pieces\n")
            if attributes["key"] == "real_estate_type":
              typedebien = str(attributes["value_label"])
              result[args["name"]][list_id]["typedebien"] = typedebien
              if typedebien == "Terrain":
                notesurface = 0
            else:
              if LOGFILE != "":
                LOGFILE.write(str(list_id) + " pas de type de bien\n")
        else:
          print(str(list_id) + " pas de attributes")
          
        note = int(noteprix)+int(noteannee)+int(notekm)+int(notetime)+int(notepiece)+int(notesurface)
        result[args["name"]][list_id]["note"] = note
        result[args["name"]][list_id]["noteprix"] = noteprix
        result[args["name"]][list_id]["noteannee"] = noteannee
        result[args["name"]][list_id]["notekm"] = notekm
        result[args["name"]][list_id]["notetime"] = notetime
        result[args["name"]][list_id]["notepiece"] = notepiece
        result[args["name"]][list_id]["notesurface"] = notesurface
        result[args["name"]][list_id]["timeperloc"] = timeperloc
    
        if CSVFILENAME != "":
          # Si l'annonce n'est pas deja dans le fichier
          if str("\"" + titre +"\";")  not in open(CSVFILENAME,"r"):
            #On regarde si elle contient les mots exclus
            try:
              for excludeword in args["exclude"]:
                if str(excludeword) in str(detail):
                  #Si un mot à exclure apparait dans le detail alors on ajoute 1 à la variable exclude
                  exclude = exclude + 1
                  if LOGFILE != "":
                    LOGFILE.write(str(list_id) + " mot cle a exclure\n")
                # Si variable exclude < 1 alors les mots à exclure ne sont pas présent dans le details de l'annonce
              if exclude < 1:
                #Alors on ecris l'annonce
                with open(CSVFILENAME,"a", encoding='utf-8') as CSVFile:
                  CSVFile.write("\"" + titre.replace('"','') + "\";\"" + str(prix).replace('"','') + "\";\"" + marque.replace('"','') + "\";\"" + modele.replace('"','') + "\";\"" + annee.replace('"','') + "\";\"" + miseencircu.replace('"','') + "\";\"" + kilometrage.replace('"','') + "\";\"" + carburant.replace('"','') + "\";\"" + boitevitesse.replace('"','') + "\";\"" + detail.replace('"','') + "\";\"" + typedebien + "\";\"" + typedevente + "\";\"" + str(pieceann) + "\";\"" + str(surface) + "\";\"" + ville.replace('"','') + "\";\"" + departement.replace('"','') + "\";\"" + region.replace('"','') + "\";\"" + lien.replace('"','') + "\";\"" + str(note) + "\";\"" + str(noteannee) + "\";\"" + str(notekm) + "\";\"" + str(noteprix) + "\";\"" + str(notetime) + "\";\"" + str(notesurface) + "\";\"" + str(notepiece) + "\"\n")
                  CSVFile.close()
              else:
                if LOGFILE != "":
                  LOGFILE.write(str(list_id) + " exclut par mot cle\n")
            except KeyError:
              with open(CSVFILENAME,"a", encoding='utf-8') as CSVFile:
                CSVFile.write("\"" + titre.replace('"','') + "\";\"" + str(prix).replace('"','') + "\";\"" + marque.replace('"','') + "\";\"" + modele.replace('"','') + "\";\"" + annee.replace('"','') + "\";\"" + miseencircu.replace('"','') + "\";\"" + kilometrage.replace('"','') + "\";\"" + carburant.replace('"','') + "\";\"" + boitevitesse.replace('"','') + "\";\"" + detail.replace('"','') + "\";\"" + typedebien + "\";\"" + typedevente + "\";\"" + str(pieceann) + "\";\"" + str(surface) + "\";\"" + ville.replace('"','') + "\";\"" + departement.replace('"','') + "\";\"" + region.replace('"','') + "\";\"" + lien.replace('"','') + "\";\"" + str(note) + "\";\"" + str(noteannee) + "\";\"" + str(notekm) + "\";\"" + str(noteprix) + "\";\"" + str(notetime) + "\";\"" + str(notesurface) + "\";\"" + str(notepiece) + "\"\n")
                CSVFile.close()
          else:
            if LOGFILE != "":
              LOGFILE.write(str(list_id) + " annonce deja presente\n")
      else:
        if LOGFILE != "":
          LOGFILE.write(str(list_id) + " prix > max ou < min\n")
    else:
      if LOGFILE != "":
        LOGFILE.write(str(list_id) + " pas de prix\n")
    #Suppression du dictionnaire propre à l'annonce si il est vide (hors critere donc)
    if len(result[args["name"]][list_id]) == 0:
      del(result[args["name"]][list_id])
    if LOGFILE != "":
      LOGFILE.write("NOUVEL ANNONCE : " + str(new_jsondata[list_id]) +"\n############################################################################################################################\n\n############################################################################################################################\n\n############################################################################################################################\n\n############################################################################################################################\n")

  resultfilename = os.path.join(PATH + 'webapp/html', ''+ args["name"] +'.html')
  with open(resultfilename, 'w', encoding='utf-8') as fr:
    fr.write(resulttemplate.render(
      search = args["name"],
      result = result,
      ymlfilename = YMLFILENAME
    ))
  
os.chdir(PATH + "webapp/html")
with http.server.HTTPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
print("fin")
