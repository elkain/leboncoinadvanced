# Image de base
FROM python:3.8

COPY app_resources/requirements.txt /tmp/

# Installation des dépendances
RUN pip install -r /tmp/requirements.txt

# Add wait-for-it
COPY wait-for-it.sh /usr/wait-for-it.sh 
RUN chmod +x /usr/wait-for-it.sh

WORKDIR /app
# On expose le port 8080
EXPOSE 8080

# On lance le serveur quand on démarre le conteneur
#ENTRYPOINT [ "python", "LBC_advancedSearch.py", "--file" ]
ENTRYPOINT [ "/usr/wait-for-it.sh", "--timeout=0", "osrm:5000", "--", "python", "LBC_advancedSearch.py", "--file" ]
CMD [ "search.yml" ]
