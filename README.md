# Leboncoin advanced search

This tool perform an advanced search for the french website : leboncoin.fr

## Features
Features provided are:
*  [x] Multiple keywords.
*  [x] Sort result following a ranking on multiple information.
  * *    [x] Price
  * *    [x] Location (In time not km) from multiple location point (closest or average for all location)
  * *    [x] Year (for vehicles)
  * *    [x] Kilometers (for vehicles)
  * *    [x] Living space (for house)
  * *    [x] Rooms (for house)
*  [x] Priorize those ranking
*  [x] Multiple search into the same YML file
*  [ ] Highlight into the HTML result all keywords matching the search ...
 
## Installation
*  Install docker.
*  Clone this repo.
*  Modify the file ```APP/app_resources/search.yml``` with your search needs.
*  Optionnal : if you want to update the map (not really useful) : ```docker build OSRM -f OSRM/osrm.Dockerfile``` (/!\ VERY LONG!!)
*  Start the stack : ```docker up -d```
*  Browse the result : ```http://localhost:8080```

## Running
### From file
To launch it you have to create a xml file with the configuration of your research.  
It must looks like that:
```
searchname:
  - name: "exemple for a car"   # A name for this search
    searchwords:
      - "Nissan GTX"            # First keywords must be into the details or into the title
      - "nismo"                 # Second keywords must be into the details or into the title
      - ""
    annee:
      - 2010                    # Year of the car
    kilometre:
      - 50000                   # Kilometers
    output:
      - "Car"                   # Just a name for the export CSV
    exclude:
      - "juke"                  # First word should not be into the announce
      - "350Z"                  # Second word should not be into the announce
    prix:
      - 15000                   # Price 
    maxtime:
      - 5.5                     # Travel time for a ranking = 0 (closest high score) in hour
      origloc:
        - locid: "My House"             # First location name
          lat: "43.764569756746875"     # First location latitude
          lng: "1.2950145687321988"     # First location longitude
        - locid: "My holidays house"    # Second location name
          lat: "42.765518789213856"     # Second location latitude
          lng: "3.035693324487738"      # Second location longitude
    locOr:
      - true                    # If true, the ranking for travel time use the closest location.
    locAnd:
      - false                   # If true, the ranking for travel time use the average time for all location.
    subject:
      - true                    # if "true" searchwords must be into the title of the announce, false into the details.

  - name: "exemple for a house"   # a name for this search
    searchwords:
      - "etage"
      - "jardin"
    output:
      - "house"
    maxtime:
      - 0,5
      origloc:
        - locid: "My job"
          lat: "43.769744456746875"
          lng: "1.2950145687321988"
        - locid: "Wife's job"           # Second location name
          lat: "42.765518789213856"     # Second location latitude
          lng: "3.035693324487738"      # Second location longitude
    subject:
      - false
    locAnd:
      - true                   # If true, the ranking for travel time use the average time for all location.
```

## Result
The result is a website to browse ``` http://localhost:8080 ```.
